import csv

file = open('scr.bas', 'w')
lineno = 10
with open('screen.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        line = ''
        lineno+=1
        print(row)
        for item in row:
            if len(item)>0:
                line += chr(int(item))
        print(line)
        file.write(str(lineno)+'PRINT"'+line+'";\r\n')
file.close()        